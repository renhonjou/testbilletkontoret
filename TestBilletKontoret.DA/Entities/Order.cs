﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Enums;

namespace TestBilletKontoret.DA.Entities
{
    [Table("orders")]
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public Int32 Id { get; set; }
        [Column("state")]
        public OrderState State { get; set; }
        [Column("number")]
        public Int32 Number { get; set; }
        [Column("creation_date")]
        public DateTime CreationDate { get; set; }
        [MaxLength(200)]
        [Column("description")]
        public String Description { get; set; }

        [Column("customer_id")]
        [Required]
        public Int32 CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        public virtual ICollection<OrderLine> Lines { get; set; }
    }
}
