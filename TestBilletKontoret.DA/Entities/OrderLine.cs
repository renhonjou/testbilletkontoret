﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Enums;

namespace TestBilletKontoret.DA.Entities
{
    [Table("order_lines")]
    public class OrderLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public Int32 Id { get; set; }
        [Column("state")]
        public OrderLineState State { get; set; }
        //? product id ?
        [Column("product_name")]
        [Required]
        public String ProductName { get; set; }
        [Column("count")]
        [Required]
        public Int32 Count { get; set; }
        [Column("price")]
        [Required]
        public Decimal Price { get; set; }

        [Column("order_id")]
        [Required]
        public Int32 OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
    }
}
