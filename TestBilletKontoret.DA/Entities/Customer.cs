﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBilletKontoret.DA.Entities
{
    [Table("customers")]
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public Int32 Id { get; set; }
        [MaxLength(100)]
        [Column("name")]
        public String Name { get; set; }
        [MaxLength(100)]
        [Column("address")]
        public String Address { get; set; }
        [MaxLength(50)]
        [Column("login")]
        [Required]
        public String Login { get; set; }
        [MaxLength(50)]
        [Column("password")]
        [Required]
        public String Password { get; set; }
    }
}
