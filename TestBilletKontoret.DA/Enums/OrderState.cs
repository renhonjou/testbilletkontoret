﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBilletKontoret.DA.Enums
{
    public enum OrderState : byte
    {
        Draft = 0,
        InProcess = 1,
        Done = 2
    }
}
