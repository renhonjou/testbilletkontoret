﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBilletKontoret.DA.Repositories
{
    public abstract class BaseRepository
    {
        protected void Transaction(Action<OrderContext> action)
        {
            using(var db = new OrderContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        action.Invoke(db);
                        transaction.Commit();
                    }
                    catch(Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        protected T Transaction<T>(Func<OrderContext, T> func)
        {
            using(var db = new OrderContext())
            {
                using(var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var result = func.Invoke(db);
                        transaction.Commit();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }
    }
}
