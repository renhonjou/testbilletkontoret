﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;
using TestBilletKontoret.DA.Interfaces;

namespace TestBilletKontoret.DA.Repositories
{
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        public Int32 Add(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            return this.Transaction<Int32>(context =>
            {
                context.Customers.Add(customer);
                context.SaveChanges();
                return customer.Id;
            });
        }

        public Customer Get(Int32 customer_id)
        {
            return this.Transaction<Customer>(context =>
            {
                return context.Customers.FirstOrDefault(c => c.Id == customer_id);
            });
        }

        public Customer Get(String login, String password = "")
        {
            if(String.IsNullOrWhiteSpace(login))
            {
                return null;
            }
            return this.Transaction<Customer>(context =>
            {
                var query = context.Customers.Where(c => c.Login == login);
                if(!String.IsNullOrWhiteSpace(password))
                {
                    return query.FirstOrDefault(c => c.Password == password);
                }
                return query.FirstOrDefault();
            });
        }

        public void Update(Customer customer)
        {
            if(customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            this.Transaction(context =>
            {
                var customer_entity = context.Customers.FirstOrDefault(c => c.Id == customer.Id);
                if (customer_entity == null)
                {
                    throw new Exception("Customer could not be found");
                }
                customer_entity.Name = customer.Name;
                customer_entity.Address = customer.Address;
                context.SaveChanges();
            });
        }
    }
}
