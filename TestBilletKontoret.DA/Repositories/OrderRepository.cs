﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;
using TestBilletKontoret.DA.Interfaces;

namespace TestBilletKontoret.DA.Repositories
{
    public class OrderRepository : BaseRepository, IOrderRepository
    {
        public void Add(Order order)
        {
            if(order == null)
            {
                throw new ArgumentNullException("order");
            }
            this.Transaction(context =>
            {
                order.CreationDate = DateTime.UtcNow;
                context.Orders.Add(order);
                context.SaveChanges();
            });
        }

        public IEnumerable<Order> Get(Int32? customer_id = null)
        {
            return this.Transaction<IEnumerable<Order>>(context =>
            {
                var orders = context.Orders;
                if (customer_id.HasValue)
                {
                    orders.Where(o => o.CustomerId == customer_id.Value);
                }
                return orders.ToList();
            });
        }

        public Order Get(Int32 order_id)
        {
            return this.Transaction<Order>(context =>
            {
                return context.Orders.FirstOrDefault(o => o.Id == order_id);
            });
        }

        public IEnumerable<Order> GetAll(Int32? customer_id = null)
        {
            return this.Transaction<IEnumerable<Order>>(context =>
            {
                var orders = context.Orders.AsQueryable();
                if(customer_id.HasValue)
                {
                    orders = orders.Where(o => o.CustomerId == customer_id.Value);
                }
                return orders.ToList();
            });
        }

        public void Update(Order order)
        {
            if(order == null)
            {
                throw new ArgumentNullException("order");
            }
            this.Transaction(context =>
            {
                context.Orders.Attach(order);
                context.Entry(order).State = EntityState.Modified;
                context.SaveChanges();
            });
        }
    }
}
