﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;
using TestBilletKontoret.DA.Interfaces;

namespace TestBilletKontoret.DA.Repositories
{
    public class OrderLineRepository : BaseRepository, IOrderLineRepository
    {
        public void Add(OrderLine order_line)
        {
            if (order_line == null)
            {
                throw new ArgumentNullException("order_line");
            }
            this.Transaction(context =>
            {
                var order = context.Orders.FirstOrDefault(o => o.Id == order_line.OrderId);
                if(order == null)
                {
                    throw new Exception("Order ID could not be found");
                }
                context.OrderLines.Add(order_line);
                context.SaveChanges();
            });
        }

        public void Add(IEnumerable<OrderLine> order_lines)
        {
            if (order_lines == null)
            {
                throw new ArgumentNullException("order_lines");
            }
            this.Transaction(context =>
            {
                foreach(var order_line in order_lines)
                {
                    var order = context.Orders.FirstOrDefault(o => o.Id == order_line.OrderId);
                    if (order == null)
                    {
                        throw new Exception("Order ID could not be found");
                    }
                    context.OrderLines.Add(order_line);
                    context.SaveChanges();
                }
            });
        }

        public IEnumerable<OrderLine> GetAll(Int32? order_id = null)
        {
            return this.Transaction<IEnumerable<OrderLine>>(context =>
            {
                var lines = context.OrderLines.AsQueryable();
                if(order_id.HasValue)
                {
                    lines = lines.Where(l => l.OrderId == order_id.Value);
                }
                return lines.ToList();
            });
        }

        public OrderLine Get(Int32 order_line_id)
        {
            return this.Transaction<OrderLine>(context =>
            {
                return context.OrderLines.FirstOrDefault(l => l.Id == order_line_id);
            });
        }

        public void Update(OrderLine order_line)
        {
            if (order_line == null)
            {
                throw new ArgumentNullException("order_line");
            }
            this.Transaction(context =>
            {
                var order = context.Orders.FirstOrDefault(o => o.Id == order_line.OrderId);
                if (order == null)
                {
                    throw new Exception("Order ID could not be found");
                }
                context.OrderLines.Attach(order_line);
                context.Entry(order_line).State = EntityState.Modified;
                context.SaveChanges();
            });
        }

        public void Update(IEnumerable<OrderLine> order_lines)
        {
            if (order_lines == null)
            {
                throw new ArgumentNullException("order_line");
            }
            this.Transaction(context =>
            {
                foreach(var order_line in order_lines)
                {
                    var order = context.Orders.FirstOrDefault(o => o.Id == order_line.OrderId);
                    if (order == null)
                    {
                        throw new Exception("Order ID could not be found");
                    }
                    context.OrderLines.Attach(order_line);
                    context.Entry(order_line).State = EntityState.Modified;
                    context.SaveChanges();
                }
            });
        }
    }
}
