﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;

namespace TestBilletKontoret.DA.Interfaces
{
    public interface IOrderLineRepository
    {
        void Add(OrderLine order_line);

        void Add(IEnumerable<OrderLine> order_lines);

        OrderLine Get(Int32 order_line_id);

        IEnumerable<OrderLine> GetAll(Int32? order_id = null);

        void Update(OrderLine order_line);

        void Update(IEnumerable<OrderLine> order_lines);
    }
}
