﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;

namespace TestBilletKontoret.DA.Interfaces
{
    public interface ICustomerRepository
    {
        Int32 Add(Customer customer);

        void Update(Customer customer);

        Customer Get(Int32 customer_id);

        Customer Get(String login, String password = "");
    }
}
