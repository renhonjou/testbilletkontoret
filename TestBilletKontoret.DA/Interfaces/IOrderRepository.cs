﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;

namespace TestBilletKontoret.DA.Interfaces
{
    public interface IOrderRepository
    {
        void Add(Order order);

        Order Get(Int32 order_id);

        IEnumerable<Order> GetAll(Int32? customer_id = null);

        void Update(Order order);
    }
}
