﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TestBilletKontoret.BL.Interfaces;
using TestBilletKontoret.BL.Services;
using TestBilletKontoret.DA.Interfaces;
using TestBilletKontoret.DA.Repositories;

namespace TestBilletKontoret.UI.Infrastructure.IoC
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            this._kernel = kernel;
            AddBindings();
        }

        public object GetService(Type service_type)
        {
            return this._kernel.TryGet(service_type);
        }

        public IEnumerable<object> GetServices(Type service_type)
        {
            return this._kernel.GetAll(service_type);
        }

        private void AddBindings()
        {
            this._kernel.Bind<IOrderRepository>().To<OrderRepository>();
            this._kernel.Bind<IOrderLineRepository>().To<OrderLineRepository>();
            this._kernel.Bind<ICustomerRepository>().To<CustomerRepository>();

            this._kernel.Bind<IOrderService>().To<OrderService>();
            this._kernel.Bind<IOrderLineService>().To<OrderLineService>();
            this._kernel.Bind<ICustomerService>().To<CustomerService>();
            this._kernel.Bind<IAuthService>().To<AuthService>();
        }
    }
}
