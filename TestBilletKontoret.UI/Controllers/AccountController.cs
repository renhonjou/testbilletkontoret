﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TestBilletKontoret.BL.Interfaces;
using TestBilletKontoret.UI.Models;

namespace TestBilletKontoret.UI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthService _authService;

        public AccountController(IAuthService auth_service)
        {
            this._authService = auth_service;
        }

        // GET: Account
        public ActionResult Login()
        {
            var model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, String ReturnUrl)
        {
            if(!ModelState.IsValid)
            {
                return View(model);
            }
            var customer = this._authService.Auth(model.Login, model.Password);
            if(customer == null)
            {
                ModelState.AddModelError(String.Empty, "Login or password isn't correct");
                return View(model);
            }

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(customer.Login, true, 20);
            var encryptedTicket = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            Response.Cookies.Add(cookie);

            return Redirect(ReturnUrl);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return Redirect("~/");
        }
    }
}