﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestBilletKontoret.BL.Interfaces;
using TestBilletKontoret.BL.Models;
using TestBilletKontoret.BL.Services;
using TestBilletKontoret.DA.Enums;
using TestBilletKontoret.DA.Repositories;

namespace TestBilletKontoret.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IOrderLineService _orderLineService;

        public HomeController(IOrderService order_service, IOrderLineService order_line_service)
        {
            this._orderService = order_service;
            this._orderLineService = order_line_service;
        }

        // GET: Home
        public ActionResult Index()
        {
            this._orderService.GetAll(1);
            //var order = this._orderService.Get(2);
            //this._orderService.Update(new OrderModel
            //{
            //    Id = 2,
            //    Number = 1,
            //    State = OrderState.InProcess,
            //    CustomerId = 1,
            //    Description = "des",
            //    CreationDate = new DateTime(2016, 10, 16),
            //    Lines = new List<OrderLineModel>
            //    {
            //        new OrderLineModel
            //        {
            //            Id = 1,
            //            Count = 2,
            //            Price = 100,
            //            ProductName = "Тарелка",
            //            State = OrderLineState.Draft,
            //        }
            //    }
            //}, true);
            //this._orderService.UpdateLine(new List<OrderLineModel>
            //{
            //    new OrderLineModel
            //    {
            //        Id = 2,
            //        OrderId = 4,
            //        Count = 3,
            //        Price = 11,
            //        ProductName = "Скатерть",
            //        State = OrderLineState.Draft,
            //    },
            //    new OrderLineModel
            //    {
            //        Id = 3,
            //        OrderId = 4,
            //        Count = 2,
            //        Price = 50,
            //        ProductName = "Микроволновка",
            //        State = OrderLineState.InProcess,
            //    }
            //});
            //order = this._orderService.Get(2);
            return View();
        }
    }
}