﻿var module = angular.module('services');

module.factory('apiService', ['$http', function ($http) {

    function getOrder(order_id) {
        return $http({
            method: 'POST',
            url: 'api/getorder',
            params: { id: order_id }
        }).then(function success(response) {
            return response.data;
        }, function error(response) {

        });
    }

    function getOrders() {
        return $http({
            method: 'POST',
            url: 'api/getorders'
        }).then(function success(response) {
            return response.data;
        }, function error(response) {

        });
    }

    function getOrderStates() {
        return $http({
            method: 'POST',
            url: 'api/getorderstates'
        }).then(function success(response) {
            return response.data;
        }, function error(response) {

        });
    };


    function getLineStates() {
        return $http({
            method: 'POST',
            url: 'api/getlinestates'
        }).then(function success(response) {
            return response.data;
        }, function error(response) {

        });
    }
    
    function getCustomerDetails() {
        return $http({
            method: 'POST',
            url: 'api/getcustomerdetails'
        }).then(function success(response) {
            return response.data;
        }, function error(response) {

        });
    }

    function updateOrderState(order_id, key) {
        $http({
            method: 'POST',
            url: 'api/updatestate',
            params: { id: order_id, key: key }
        }).then(function success(response) {

        }, function error(response) {

        });
    }

    function updateLineState(line_id, key) {
        $http({
            method: 'POST',
            url: 'api/updatelinestate',
            params: { id: line_id, key: key }
        }).then(function success(response) {

        }, function error(response) {

        });
    }

    return {
        getOrder: getOrder,
        getOrders: getOrders,
        getOrderStates: getOrderStates,
        getLineStates: getLineStates,
        updateOrderState: updateOrderState,
        updateLineState: updateLineState,
        getCustomerDetails: getCustomerDetails
    };
}]);