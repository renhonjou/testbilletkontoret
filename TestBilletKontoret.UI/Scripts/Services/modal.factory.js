﻿var module = angular.module('services');

module.factory('modal', function (btfModal) {
    return btfModal({
        controller: 'ModalController',
        controllerAs: 'modal',
        templateUrl: '/content/templates/modal.html'
    });
});