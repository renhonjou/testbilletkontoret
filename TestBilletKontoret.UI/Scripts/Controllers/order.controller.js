﻿var module = angular.module('controllers');

module.controller('OrderController', ['$scope', 'modal', 'apiService', OrderController]);

function OrderController($scope, modal, apiService) {
    var self = this,
        OrderLineStates,
        OrderStates;

    function getFromArrayById(array, id) {
        return array.find(function (element) {
            return element.Id === id;
        });
    }

    function getState(states, key) {
        return states.find(function (element) {
            return element.Key === key;
        });
    }

    function filterLinesByState(lines, key) {
        var index = lines.length;
        var result = [];

        do {
            index--;
            if (lines[index].State == key) {
                result.push(lines[index]);
            }
        } while (index != 0)

        return result;
    }

    function setStateNames(states, array) {
        if (!states) {
            return 'Undefined';
        }
        var index = array.length;
        do {
            index--;
            var state_object = getState(states, array[index].State);
            if (state_object) {
                array[index].StateName = state_object.Value;
            }
            else {
                array[index].StateName = 'Undefined';
            }
        } while (index != 0)
    }

    function setOrdersSummary() {
        if (!$scope.Orders || !self.OrderLineStates) {
            return 'Undefined';
        }
        var order_index = $scope.Orders.length;

        do {
            order_index--;
            updateOrderSummary($scope.Orders[order_index]);
        } while(order_index != 0)
    }

    function updateOrderSummary(order) {
        order.Summary = "";
        if (!order.Lines) {
            order.Summary = "No Lines";
            return;
        }
        var state_index = self.OrderLineStates.length;
        do {
            state_index--;
            var line_objects = filterLinesByState(order.Lines, self.OrderLineStates[state_index].Key);
            order.Summary = order.Summary + self.OrderLineStates[state_index].Value + " - " + line_objects.length + " ";
        } while (state_index != 0)
    }

    var promise = apiService.getOrders();
    promise.then(function (orders) {
        $scope.Orders = orders;
        var orderStatesPromise = apiService.getOrderStates();
        orderStatesPromise.then(function (states) {
            self.OrderStates = states;
            setStateNames(self.OrderStates, $scope.Orders);
        });
        var lineStatesPromise = apiService.getLineStates();
        lineStatesPromise.then(function (states) {
            self.OrderLineStates = states;
            setOrdersSummary();
        });
    });

	$scope.ShowOrder = function (order_id) {
	    var order = getFromArrayById($scope.Orders, order_id);
	    modal.data = { Order: order, OrderStates: self.OrderStates, OrderLineStates: self.OrderLineStates };
	    modal.getState = getState;
	    modal.updateOrderSummary = updateOrderSummary;
	    modal.activate();
	}
}