﻿var module = angular.module('controllers');

module.controller('ModalController', ['$scope', 'modal', 'apiService', ModalController]);

function ModalController($scope, modal, apiService) {
    var self = this;

    if (!modal.data.Order) {
        modal.deactivate();
    }
    self.Order = modal.data.Order;
    self.OrderStates = modal.data.OrderStates;
    self.OrderLineStates = modal.data.OrderLineStates;

    //update states
    this.updateOrderState = function () {
        apiService.updateOrderState(self.Order.Id, self.Order.State);
        var state_object = modal.getState(self.OrderLineStates, self.Order.State);
        if (state_object) {
            self.Order.StateName = state_object.Value;
        }
    }
    this.updateOrderLineState = function (line) {
        apiService.updateLineState(line.Id, line.State);
        modal.updateOrderSummary(self.Order);
    }

    //close modal
    this.close = modal.deactivate;
}