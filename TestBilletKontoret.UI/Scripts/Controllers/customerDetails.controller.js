﻿var module = angular.module('controllers');

module.controller('CustomerDetailsController', ['$scope', 'apiService', CustomerDetailsController]);

function CustomerDetailsController($scope, apiService) {
    var promise = apiService.getCustomerDetails();
    promise.then(function (details) {
        $scope.CustomerDetails = details;
    });
}