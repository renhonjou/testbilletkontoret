﻿angular.module('controllers', []);
angular.module('filters', []);
angular.module('services', ['btford.modal'])

var app = angular.module('orders', ['controllers', 'filters', 'services', 'ngRoute']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/orders', { templateUrl: '/content/views/orders.html', controller: 'OrderController' });
    $routeProvider.when('/customer-details', { templateUrl: '/content/views/customer-details.html', controller: 'CustomerDetailsController' });
    $routeProvider.otherwise({ redirectTo: '/orders' });
});