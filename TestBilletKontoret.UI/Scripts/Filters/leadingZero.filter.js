﻿var module = angular.module('filters');

module.filter('leadingZero', function() {
	return function(number_value, length_value) {
		var number = parseInt(number_value, 10);
		var target_length = parseInt(length_value, 10);
		if(isNaN(number) || isNaN(target_length)) {
			return number_value;
		}
		//convert to string;
		number = '' + number;
	    while(number.length < target_length) {
			number = '0' + number;
		}
		return '#' + number;
	};
});