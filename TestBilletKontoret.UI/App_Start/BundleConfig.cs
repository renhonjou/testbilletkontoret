﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace TestBilletKontoret.UI.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                "~/Scripts/Vendors/angular.min.js", 
                "~/Scripts/Vendors/angular-route.min.js", 
                "~/Scripts/Vendors/modal.min.js"));
            

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/app.js",
                "~/Scripts/Controllers/order.controller.js",
                "~/Scripts/Controllers/customerDetails.controller.js",
                "~/Scripts/Controllers/modal.controller.js",
                "~/Scripts/Services/apiService.factory.js",
                "~/Scripts/Services/modal.factory.js",
                "~/Scripts/Filters/leadingZero.filter.js"));
        }
    }
}