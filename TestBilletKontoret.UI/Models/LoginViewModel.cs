﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestBilletKontoret.UI.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "*Required")]
        [Display(Name = "Login")]
        public String Login { get; set; }
        [Required(ErrorMessage = "*Required")]
        [Display(Name = "Password")]
        public String Password { get; set; }
    }
}