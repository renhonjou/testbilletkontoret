﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using TestBilletKontoret.BL.Interfaces;
using TestBilletKontoret.DA.Enums;

namespace TestBilletKontoret.UI.Api
{
    public class ApiController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IOrderLineService _orderLineService;
        private readonly ICustomerService _customerService;

        public ApiController(IOrderService order_service, ICustomerService customer_service, IOrderLineService order_line_service)
        {
            this._orderService = order_service;
            this._orderLineService = order_line_service;
            this._customerService = customer_service;
        }
        
        [HttpPost]
        public JsonResult GetOrders()
        {
            if (!User.Identity.IsAuthenticated) throw new Exception();
            var customer = this._customerService.Get(User.Identity.Name);
            var orders = this._orderService.GetAll(customer.Id);
            if (orders == null) throw new ArgumentException("orders");
            return Json(orders.Select(o => new {
                o.Id,
                o.State,
                o.Number,
                o.Amount,
                CreationDate = o.CreationDate.ToString("f", CultureInfo.GetCultureInfo("en-US")),
                LifeTime = String.Format("{0} day(s) {1} hours", o.LifeTime.ToString("%d", CultureInfo.GetCultureInfo("en-US")), o.LifeTime.ToString("%h", CultureInfo.GetCultureInfo("en-US"))),
                o.Description,
                Lines = o.Lines != null && o.Lines.Any() ? o.Lines.Select(l => new {
                    l.Id,
                    l.State,
                    l.ProductName,
                    l.OrderId,
                    l.Count,
                    l.Price,
                    l.Total
                }) : null
            }));
        }

        [HttpPost]
        public JsonResult GetOrderStates()
        {
            if (!User.Identity.IsAuthenticated) throw new Exception();
            return Json(new List<object> {
                new { Key = (byte)OrderState.Draft, Value = OrderState.Draft.ToString() },
                new { Key = (byte)OrderState.InProcess, Value = OrderState.InProcess.ToString() },
                new { Key = (byte)OrderState.Done, Value = OrderState.Done.ToString() },
            });
        }

        [HttpPost]
        public JsonResult GetLineStates()
        {
            if (!User.Identity.IsAuthenticated) throw new Exception();
            return Json(new List<object> {
                new { Key = (byte)OrderLineState.Draft, Value = OrderLineState.Draft.ToString() },
                new { Key = (byte)OrderLineState.InProcess, Value = OrderLineState.InProcess.ToString() },
                new { Key = (byte)OrderLineState.Done, Value = OrderLineState.Done.ToString() },
            });
        }

        [HttpPost]
        public JsonResult UpdateState(Int32 id, Byte key)
        {
            if (!User.Identity.IsAuthenticated) throw new Exception();
            var order = this._orderService.Get(id);
            if (order == null) return null;
            if (!Enum.IsDefined(typeof(OrderState), key)) throw new ArgumentException("key");
            var state = (OrderState)key;
            order.State = state;
            var result = this._orderService.Update(order);
            return Json(new { success = result });
        }

        [HttpPost]
        public JsonResult UpdateLineState(Int32 id, Byte key)
        {
            if (!User.Identity.IsAuthenticated) throw new Exception();
            var line = this._orderLineService.Get(id);
            if (line == null) return null;
            if (!Enum.IsDefined(typeof(OrderLineState), key)) throw new ArgumentException("key");
            var state = (OrderLineState)key;
            line.State = state;
            var result = this._orderLineService.Update(line);
            return Json(new { success = result });
        }

        [HttpPost]
        public JsonResult GetCustomerDetails()
        {
            if (!User.Identity.IsAuthenticated) throw new Exception();
            var customer = this._customerService.Get(User.Identity.Name);
            return Json(new { customer.Name, customer.Address });
        }
    }
}
