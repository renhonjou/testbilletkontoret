﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.BL.Models;

namespace TestBilletKontoret.BL.Interfaces
{
    public interface IOrderService
    {
        OrderModel Get(Int32 order_id);

        IEnumerable<OrderModel> GetAll(Int32? customer_id = null);

        Boolean Add(OrderModel order);

        Boolean Update(OrderModel order, Boolean with_line = false);
    }
}
