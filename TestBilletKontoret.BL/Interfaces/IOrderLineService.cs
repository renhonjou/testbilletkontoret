﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.BL.Models;

namespace TestBilletKontoret.BL.Interfaces
{
    public interface IOrderLineService
    {
        OrderLineModel Get(Int32 order_line_id);
        
        Boolean Add(OrderLineModel order_line);

        Boolean Add(IEnumerable<OrderLineModel> order_lines);

        Boolean Update(OrderLineModel order_line);

        Boolean Update(IEnumerable<OrderLineModel> order_lines);
    }
}
