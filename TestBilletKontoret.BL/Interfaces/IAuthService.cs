﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.BL.Models;

namespace TestBilletKontoret.BL.Interfaces
{
    public interface IAuthService
    {
        CustomerModel Auth(String login, String password);
    }
}
