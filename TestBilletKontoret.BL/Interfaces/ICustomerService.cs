﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.BL.Models;

namespace TestBilletKontoret.BL.Interfaces
{
    public interface ICustomerService
    {
        Boolean Add(CustomerModel customer);

        void Update(CustomerModel customer);

        CustomerModel Get(Int32 customer_id);

        CustomerModel Get(String login);
    }
}
