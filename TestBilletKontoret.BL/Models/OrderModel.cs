﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;
using TestBilletKontoret.DA.Enums;

namespace TestBilletKontoret.BL.Models
{
    public class OrderModel
    {
        public Int32 Id { get; set; }
        public OrderState State { get; set; }
        public Int32 Number { get; set; }
        public DateTime CreationDate { get; set; }
        public TimeSpan LifeTime { get; set; }
        public Decimal Amount
        {
            get
            {
                return Lines != null ? Lines.Sum(l => l.Total) : 0;
            }
        }
        public String Description { get; set; }

        public Int32 CustomerId { get; set; }
        public IEnumerable<OrderLineModel> Lines { get; set; }

        public OrderModel() { }

        public OrderModel(Order entity)
        {
            this.Id = entity.Id;
            this.State = entity.State;
            this.Number = entity.Number;
            this.CreationDate = entity.CreationDate;
            this.CustomerId = entity.CustomerId;
            this.Description = entity.Description;
        }

        public Order ToEntity()
        {
            return new Order
            {
                Id = this.Id,
                State = this.State,
                Number = this.Number,
                CreationDate = this.CreationDate,
                Description = this.Description,
                CustomerId = this.CustomerId
            };
        } 
    }
}
