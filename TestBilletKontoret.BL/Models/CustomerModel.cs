﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;
using TestBilletKontoret.DA.Enums;

namespace TestBilletKontoret.BL.Models
{
    public class CustomerModel
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public String Login { get; set; }
        public String Password { get; set; }

        public CustomerModel() { }

        public CustomerModel(Customer entity)
        {
            this.Id = entity.Id;
            this.Name = entity.Name;
            this.Address = entity.Address;
            this.Login = entity.Login;
            this.Password = entity.Password;
        }

        public Customer ToEntity()
        {
            return new Customer
            {
                Id = this.Id,
                Name = this.Name,
                Address = this.Address,
                Login = this.Login,
                Password = this.Password
            };
        } 
    }
}
