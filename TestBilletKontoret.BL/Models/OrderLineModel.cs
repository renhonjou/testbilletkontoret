﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.DA.Entities;
using TestBilletKontoret.DA.Enums;

namespace TestBilletKontoret.BL.Models
{
    public class OrderLineModel
    {
        public Int32 Id { get; set; }
        public OrderLineState State { get; set; }
        public String ProductName { get; set; }
        public Int32 Count { get; set; }
        public Decimal Price { get; set; }
        public Decimal Total
        {
            get
            {
                return Price * Count;
            }
        }
        public Int32 OrderId { get; set; }

        public OrderLineModel() { }

        public OrderLineModel(OrderLine entity)
        {
            this.Id = entity.Id;
            this.State = entity.State;
            this.ProductName = entity.ProductName;
            this.Count = entity.Count;
            this.Price = entity.Price;
            this.OrderId = entity.OrderId;
        }

        public OrderLine ToEntity()
        {
            return new OrderLine
            {
                Id = this.Id,
                State = this.State,
                ProductName = this.ProductName,
                Count = this.Count,
                Price = this.Price,
                OrderId = this.OrderId
            };
        }
    }
}
