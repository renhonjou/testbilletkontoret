﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.BL.Interfaces;
using TestBilletKontoret.BL.Models;
using TestBilletKontoret.DA.Entities;
using TestBilletKontoret.DA.Interfaces;

namespace TestBilletKontoret.BL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderLineRepository _orderLineRepository;

        public OrderService(IOrderRepository order_repository, IOrderLineRepository order_line_repository)
        {
            this._orderRepository = order_repository;
            this._orderLineRepository = order_line_repository;
        }

        public Boolean Add(OrderModel order)
        {
            if (order == null)
            {
                throw new ArgumentNullException("order");
            }
            try
            {
                var order_entity = order.ToEntity();
                this._orderRepository.Add(order_entity);
                if (order.Lines != null && order.Lines.Any())
                {
                    var ol_entities = order.Lines.Select(ol => { ol.OrderId = order_entity.Id; return ol.ToEntity(); }).ToList();
                    this._orderLineRepository.Add(ol_entities);
                }
            }
            catch (Exception ex)
            {
                //TODO: log
                return false;
            }
            return true;
        }

        public Boolean Update(OrderModel order, Boolean with_line = false)
        {
            if (order == null)
            {
                throw new ArgumentNullException("order");
            }
            try
            {
                var order_entity = order.ToEntity();
                this._orderRepository.Update(order_entity);
                if (with_line && order.Lines != null && order.Lines.Any())
                {
                    var ol_entities = order.Lines.Select(ol => { ol.OrderId = order_entity.Id; return ol.ToEntity(); }).ToList();
                    this._orderLineRepository.Update(ol_entities);
                }
            }
            catch (Exception ex)
            {
                //TODO: log
                return false;
            }
            return true;
        }

        public OrderModel Get(Int32 order_id)
        {
            var order_entity = this._orderRepository.Get(order_id);
            if (order_entity == null) return null;
            var order = new OrderModel(order_entity) {
                Lines = new List<OrderLineModel>(),
            };

            order.CreationDate = order.CreationDate.ToLocalTime();
            order.LifeTime = DateTime.Now - order.CreationDate;

            var ol_entities = this._orderLineRepository.GetAll(order_id);
            if(ol_entities != null && ol_entities.Any())
            {
                order.Lines = ol_entities.Select(ol => new OrderLineModel(ol)).ToList();
            }
            return order;
        }

        public IEnumerable<OrderModel> GetAll(Int32? customer_id = null)
        {
            var order_entities = this._orderRepository.GetAll(customer_id);
            if (order_entities == null || !order_entities.Any()) return new List<OrderModel>();
            var orders = order_entities.Select(o => new OrderModel(o)).ToList();
            foreach(var order in orders)
            {
                order.Lines = new List<OrderLineModel>();
                order.CreationDate = order.CreationDate.ToLocalTime();
                order.LifeTime = DateTime.Now - order.CreationDate;
                var ol_entities = this._orderLineRepository.GetAll(order.Id);
                if(ol_entities != null && ol_entities.Any())
                {
                    order.Lines = ol_entities.Select(ol => new OrderLineModel(ol)).ToList();
                }
            }
            return orders;
        }
    }
}
