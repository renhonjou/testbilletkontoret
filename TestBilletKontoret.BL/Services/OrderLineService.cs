﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.BL.Interfaces;
using TestBilletKontoret.BL.Models;
using TestBilletKontoret.DA.Entities;
using TestBilletKontoret.DA.Interfaces;

namespace TestBilletKontoret.BL.Services
{
    public class OrderLineService : IOrderLineService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderLineRepository _orderLineRepository;

        public OrderLineService(IOrderRepository order_repository, IOrderLineRepository order_line_repository)
        {
            this._orderRepository = order_repository;
            this._orderLineRepository = order_line_repository;
        }

        public Boolean Add(OrderLineModel order_line)
        {
            if (order_line == null)
            {
                throw new ArgumentNullException("order_line");
            }
            try
            {
                this._orderLineRepository.Add(order_line.ToEntity());
            }
            catch (Exception ex)
            {
                //TODO: log
                return false;
            }
            return true;
        }

        public Boolean Add(IEnumerable<OrderLineModel> order_lines)
        {
            if (order_lines == null || order_lines.Count() == 0)
            {
                throw new ArgumentNullException("order_lines");
            }
            try
            {
                var ol_entities = order_lines.Select(ol => ol.ToEntity()).ToList();
                this._orderLineRepository.Add(ol_entities);
            }
            catch (Exception ex)
            {
                //TODO: log
                return false;
            }
            return true;
        }

        public Boolean Update(OrderLineModel order_line)
        {
            if (order_line == null)
            {
                throw new ArgumentNullException("order_line");
            }
            try
            {
                this._orderLineRepository.Update(order_line.ToEntity());
            }
            catch (Exception ex)
            {
                //TODO: log
                return false;
            }
            return true;
        }

        public Boolean Update(IEnumerable<OrderLineModel> order_lines)
        {
            if (order_lines == null || order_lines.Count() == 0)
            {
                throw new ArgumentNullException("order_lines");
            }
            try
            {
                var ol_entities = order_lines.Select(ol => ol.ToEntity()).ToList();
                this._orderLineRepository.Update(ol_entities);
            }
            catch (Exception ex)
            {
                //TODO: log
                return false;
            }
            return true;
        }

        public OrderLineModel Get(Int32 order_line_id)
        {
            var ol_entity = this._orderLineRepository.Get(order_line_id);
            if (ol_entity == null) return null;
            return new OrderLineModel(ol_entity);
        }
    }
}
