﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.BL.Interfaces;
using TestBilletKontoret.BL.Models;
using TestBilletKontoret.DA.Interfaces;

namespace TestBilletKontoret.BL.Services
{
    public class AuthService : IAuthService
    {
        private readonly ICustomerRepository _customerRepository;

        public AuthService(ICustomerRepository customer_repository)
        {
            this._customerRepository = customer_repository;
        }

        public CustomerModel Auth(String login, String password)
        {
            var customer_entity = this._customerRepository.Get(login, password);
            if (customer_entity == null) return null;
            return new CustomerModel(customer_entity);
        }
    }
}
