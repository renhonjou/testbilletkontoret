﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBilletKontoret.BL.Interfaces;
using TestBilletKontoret.BL.Models;
using TestBilletKontoret.DA.Interfaces;

namespace TestBilletKontoret.BL.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customer_repository)
        {
            this._customerRepository = customer_repository;
        }

        public Boolean Add(CustomerModel customer)
        {
            if(customer == null || String.IsNullOrWhiteSpace(customer.Login) || String.IsNullOrWhiteSpace(customer.Password))
            {
                throw new ArgumentNullException("customer");
            }
            var customer_entity = this._customerRepository.Get(customer.Login);
            if(customer_entity != null)
            {
                return false;
            }
            this._customerRepository.Add(customer.ToEntity());
            return true;
        } 

        public CustomerModel Get(Int32 customer_id)
        {
            var customer_entity = this._customerRepository.Get(customer_id);
            if (customer_entity == null) return null;

            return new CustomerModel(customer_entity);
        }

        public CustomerModel Get(String login)
        {
            var customer_entity = this._customerRepository.Get(login);
            if (customer_entity == null) return null;

            return new CustomerModel(customer_entity);
        }

        public void Update(CustomerModel customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            this._customerRepository.Update(customer.ToEntity());
        }
    }
}
